from DPD.dpd import dpd_generator

g = dpd_generator()
g.build_corpus("corpus_example")
g.filter_stop_words()
print(g.corpus)
g.build_vector_space_model()
print(g.word_vectors)
g.build_clusters()
print(g.clustering)
g.build_dendrograms("dendrograms/")
